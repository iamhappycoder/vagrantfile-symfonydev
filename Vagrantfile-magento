Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/xenial64"
  config.vm.box_version = "20170307.0.1"

  config.vm.network "forwarded_port", guest: 3306, host: 13306
  config.vm.network "forwarded_port", guest: 80, host: 10080
  config.vm.network "forwarded_port", guest: 443, host: 10443

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
    vb.cpus = 2
  end

  config.vm.provision "shell", inline: <<-SHELL
    #
    # Linux
    #
    apt update -y
    echo -e "127.0.0.1\tdev.book-setup.smartagents.com" | tee --append /etc/hosts > /dev/null

    #
    # MySQL
    #
    echo "mysql-server-5.7 mysql-server/root_password password root" | debconf-set-selections
    echo "mysql-server-5.7 mysql-server/root_password_again password root" | debconf-set-selections
    apt-get -y install mysql-server-5.7
    mysql -uroot -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION;"
    mysql -uroot -proot -e "FLUSH PRIVILEGES"
    sed -i 's/bind-address\t\t= 127.0.0.1/bind-address\t\t= 0.0.0.0/g' /etc/mysql/mysql.conf.d/mysqld.cnf
    service mysql restart

    #
    # PHP
    #
    apt install -y php7.0 php7.0-xml php7.0-intl php-mbstring php-zip php7.0-mysql php-xdebug php7.0-mcrypt php-memcached php-apcu php-apcu-bc-
    for sapi in fpm cli
    do
        sed -i 's/^memory_limit = 128M/memory_limit = 2G/g' /etc/php/7.0/$sapi/php.ini
        sed -i 's/^error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT/error_reporting = E_ALL/g' /etc/php/7.0/$sapi/php.ini
        sed -i 's/^;date.timezone =/date.timezone = UTC/g' /etc/php/7.0/$sapi/php.ini
    done
    echo -e "\napc.enabled=1\napc.enable_cli=1" | tee --append /etc/php/7.0/mods-available/apcu.ini > /dev/null
    echo -e "\nopcache.enable=1\nopcache.enable_cli=1" | tee --append /etc/php/7.0/mods-available/opcache.ini > /dev/null
    echo -e "\nxdebug.var_display_max_data = -1\nxdebug.var_display_max_children = -1\nxdebug.var_display_max_depth = -1\nxdebug.remote_enable = 1\nxdebug.remote_connect_back = 1" | tee --append /etc/php/7.0/mods-available/xdebug.ini > /dev/null

    #
    # Nginx
    #
    apt install -y nginx
    bash -c 'cat << EOF > /etc/nginx/sites-available/dev.book-setup.smartagents.com2
upstream fastcgi_backend {
        server   unix:/var/run/php/php7.0-fpm.sock;
}
server {
        listen 80;
        server_name dev.book-setup.smartagents.com;
        set \$MAGE_ROOT /home/ubuntu/book-setup.smartagents.com;
        include /home/ubuntu/book-setup.smartagents.com/nginx.conf;
}
EOF'
    ln -sf /etc/nginx/sites-available/dev.book-setup.smartagents.com /etc/nginx/sites-enabled/dev.book-setup.smartagents.com
    service nginx restart

    #
    # Misc
    #

    # Tools
    curl -LsS  https://getcomposer.org/download/1.4.2/composer.phar -o /usr/local/bin/composer
    chmod a+x /usr/local/bin/composer

    # MySQL database
    mysql -uroot -proot -e "create database magento"
  SHELL
end
