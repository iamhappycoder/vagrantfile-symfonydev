Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/xenial64"
  config.vm.box_version = "20170307.0.1"

  config.vm.network "forwarded_port", guest: 3306, host: 13306
  config.vm.network "forwarded_port", guest: 80, host: 10080
  config.vm.network "forwarded_port", guest: 443, host: 10443

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
    vb.cpus = 2
  end

  config.vm.provision "shell", inline: <<-SHELL
    #
    # Linux
    #
    add-apt-repository -y ppa:ondrej/php
    apt update -y

    #
    # Apache
    # 
    apt install -y apache2
    sed -i 's/export APACHE_RUN_USER=www-data/export APACHE_RUN_USER=ubuntu/g' /etc/apache2/envvars
    sed -i 's/export APACHE_RUN_GROUP=www-data/export APACHE_RUN_GROUP=ubuntu/g' /etc/apache2/envvars
    sed -i 's/AllowOverride None/AllowOverride All/g' /etc/apache2/apache2.conf
    chown -R ubuntu /var/lock/apache2/
    a2dismod mpm_event
    a2enmod mpm_prefork rewrite expires headers ssl
    a2ensite default-ssl
    service apache2 restart

    #
    # MySQL
    #
    echo "mysql-server-5.7 mysql-server/root_password password root" | sudo debconf-set-selections
    echo "mysql-server-5.7 mysql-server/root_password_again password root" | sudo debconf-set-selections
    apt-get -y install mysql-server-5.7
    mysql -uroot -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION;"
    mysql -uroot -proot -e "FLUSH PRIVILEGES"
    sed -i 's/bind-address\t\t= 127.0.0.1/bind-address\t\t= 0.0.0.0/g' /etc/mysql/mysql.conf.d/mysqld.cnf
    service mysql restart

    #
    # PHP
    #
    apt install -y php7.1 php7.1-xml php7.1-intl php-mbstring php-zip php7.1-mysql php-xdebug php7.1-mcrypt php-memcached php-apcu php-apcu-bc-
    for sapi in apache2 cli
    do
        sed -i 's/^memory_limit = 128M/memory_limit = 1G/g' /etc/php/7.1/$sapi/php.ini
        sed -i 's/^error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT/error_reporting = E_ALL/g' /etc/php/7.1/$sapi/php.ini
        sed -i 's/^;date.timezone =/date.timezone = UTC/g' /etc/php/7.1/$sapi/php.ini
    done
    echo -e "\napc.enabled=1\napc.enable_cli=1" | tee --append /etc/php/7.1/mods-available/apcu.ini > /dev/null
    echo -e "\nopcache.enable=1\nopcache.enable_cli=1" | tee --append /etc/php/7.1/mods-available/opcache.ini > /dev/null
    echo -e "\nxdebug.var_display_max_data = -1\nxdebug.var_display_max_children = -1\nxdebug.var_display_max_depth = -1\nxdebug.remote_enable = 1\nxdebug.remote_connect_back = 1" | tee --append /etc/php/7.1/mods-available/xdebug.ini > /dev/null
    service apache2 restart

    #
    # Misc
    #

    # Tools
    curl -LsS  https://getcomposer.org/download/1.4.1/composer.phar -o /usr/local/bin/composer
    curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony
    chmod a+x /usr/local/bin/composer /usr/local/bin/symfony
    
    # Symfony web dir
    mv /var/www/html /var/www/html.orig
    ln -sf /vagrant/web /var/www/html

    # MySQL database
    mysql -uroot -proot -e "create database symfony"
  SHELL
end
